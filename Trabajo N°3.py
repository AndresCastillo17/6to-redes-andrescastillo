import socket

server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

server.bind(('localHost',5656))
server.listen(5)

print("SERVIDOR ESPERANDO")

conexion,direccion = server.accept()

while True:
    respuesta = conexion.recv(1024).decode(encoding="ascii")

    print("cliente: ",respuesta)

    enviar=input("Servidor: ")

    conexion.send(enviar.encode(encoding="ascii"))
    
    breakConexion = input("Quieres terminar su conexion: si o no?")

    if (breakConexion ==  "si"):
        
        break

conexion.close()